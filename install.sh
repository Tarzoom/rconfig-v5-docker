#!/bin/sh
# Setup logging
LOGFILE=install.log
# Script credit: Danie Pham
# Script credit site: https://www.writebash.com
# Script credit source: https://gitlab.com/Danny_Pham/WriteBash.com/raw/master/Install/06-Script_install_LAMP_PHP_7.2_on_CentOS_7.sh

## Set some Colors
red=`tput setaf 1`
green=`tput setaf 2`
blue=`tput setaf 4`
magenta=`tput setaf 5`
cyan=`tput setaf 6`
reset=`tput sgr0`

function pauseForinput {
        if [ "$1" = "-i" ]; then
                read -p "Continue (y/n)?" CONT
                if [ "$CONT" == "y" ]; then
                  echo "${green}yaaa";
                else
                  exit 1
                fi
        fi
}
function pauseForinputReboot {
        if [ "$1" = "-i" ]; then
                echo "The install has completed...";
                read -p "Reboot (y/n)?" CONT
                if [ "$CONT" == "y" ]; then
                  echo "${green}yaaa";
                else
                  exit 1
                fi
        fi
}
echo "${blue}Starting rConfig installation...${reset}";
sleep 1


# Function check user root
f_check_root () {
    if (( $EUID == 0 )); then
        # If user is root, continue to function f_sub_main
        f_sub_main
    else
        # If user not is root, print message and exit script
        echo "${red}Please run this script as root !${reset}"
        exit
    fi
}

OSVERSION=$(rpm --eval '%{centos_ver}')
echo $OSVERSION

if [[ $OSVERSION -eq 7 ]]; then
    echo "${blue}The OS version is Centos "$OSVERSION${reset}
    yum -y install epel-release
    yum -y install dnf
    if ! command -v dnf &>/dev/null; then
        echo "DNF could not be found"
        exit
    fi
elif [[ $OSVERSION -eq 8 ]]; then
    echo "${blue}The OS version is Centos "$OSVERSION${reset}
else
    echo "${red}The OS version is not Centos 7 or 8.${reset}"
    exit
fi

# Function to disable SELinux
f_disable_selinux () {
    SE=`cat /etc/selinux/config | grep ^SELINUX= | awk -F'=' '{print $2}'`
    echo "${green}Checking SELinux status ...${reset}"
    echo ""
    sleep 1

    if [[ "$SE" == "enforcing" ]]; then
        sed -i 's|SELINUX=enforcing|SELINUX=disabled|g' /etc/selinux/config
        echo "${green}Disabling SElinux and rebooting server after 5s. Press Ctrl+C if you have to stop the script.${reset}"
        echo "${green}After system reboot, please run script again.${reset}"
        echo ""
        sleep 5
        reboot
    fi
}

# Function update os
f_update_os () {
    echo "${green}Starting OS update... Hold on a few minutes!${reset}"
    sleep 1

    yum update -y
    yum upgrade -y
    yum install sudo -y
    yum history sync
    yum install php -y
    yum install composer -y
    echo ""
    echo "${green}OS update completed!${reset}"
    echo ""
    sleep 1
}

f_install_base_pkgs () {
    echo "${green}Starting base package installation... Hold on a few minutes!${reset}"
    sleep 1

    yum -y groupinstall 'Development Tools'
    yum -y install ntp sudo telnet bind-utils traceroute tree zip unzip vixie-cron crontabs vim telnet
	yum -y install openssl-devel openssl mod_ssl supervisord
    dnf install git -y

    echo ""
    echo "${green}Base package installation update completed!${reset}"
    echo ""

    sleep 1
}

f_install_redis_supervisor(){
    echo "${green}Starting redis installation... please wait!${reset}"
    sudo dnf install redis -y
    sudo systemctl start redis
    sudo systemctl enable redis
    sudo systemctl status redis

    echo "${green}Starting supervisor installation... please wait!${reset}"

    yum install epel-release -y
    yum -y install supervisor
    sudo systemctl start supervisord
    sudo systemctl enable supervisord

}

f_install_lamp () {

    ########## INSTALL APACHE ##########
    echo "${green}Starting LAMP Installation...${reset}"
    sleep 1
    echo "${green}Installing apache ...${reset}"

    yum install httpd -y

    # This part is optimize for server 2GB RAM
    cp /etc/httpd/conf/httpd.conf /etc/httpd/conf/httpd.conf.original
    # sed -i '/<IfModule prefork.c/,/<\/IfModule/{//!d}' /etc/httpd/conf/httpd.conf
    # sed -i '/<IfModule prefork.c/a\ StartServers              4\n MinSpareServers           20\n MaxSpareServers           40\n MaxClients         200\n MaxRequestsPerChild    4500' /etc/httpd/conf/httpd.conf

    # Enable and start httpd service
    systemctl enable httpd.service
    systemctl restart httpd.service

    ########## INSTALL MARIADB ##########
    echo "${green}Add MariaDB to repositories ...${reset}"
    echo ""
    sleep 1

    # Add MariaDB repository
    cat > /etc/yum.repos.d/MariaDB.repo <<"EOF"
[mariadb]
name = MariaDB
baseurl = http://yum.mariadb.org/10.2/centos7-amd64
gpgkey=https://yum.mariadb.org/RPM-GPG-KEY-MariaDB
gpgcheck=1
EOF

    # Update new package
    echo "${green}Update package for MariaDB ...${reset}"
    sleep 1
    yum update -y

    # Start install MariaDB
    echo "${green}Installing MariaDB server ...${reset}"
    sleep 1
    yum install mariadb-server -y

    # Enable and start mysql service
    systemctl enable mariadb
    systemctl start mariadb
    echo ""
    sleep 1

    ########## INSTALL PHP7.4 ##########
    echo "${green}Installing PHP 7.4 ...${reset}"

    if [[ $OSVERSION -eq 7 ]]; then
        echo "${blue}Installing PHP 7.4 for Centos "$OSVERSION${reset}
        sudo yum -y install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
        sudo yum -y install https://rpms.remirepo.net/enterprise/remi-release-7.rpm
        sudo yum -y install yum-utils
        sudo yum-config-manager --enable remi-php74
        sudo yum -y update
        sudo yum -y php php-mcrypt php-cli php-gd php-curl php-mysql php-ldap php-zip php-fileinfo php-pear php-mbstring php-common php-pdo php-ldap php-gmp


    elif [[ $OSVERSION -eq 8 ]]; then
        echo "${blue}Installing PHP 7.4 for Centos "$OSVERSION${reset}
        dnf install dnf-utils http://rpms.remirepo.net/enterprise/remi-release-8.rpm -y
        dnf module reset php -y
        dnf module enable php:remi-7.4 -y
        dnf install php php-mcrypt php-cli php-gd php-curl php-mysql php-ldap php-zip php-fileinfo php-pear php-mbstring php-common php-pdo php-ldap php-gmp -y
    fi


    # Config to fix error Apache not load PHP file
    chown -R apache:apache /var/www
    sed -i '/<Directory \/>/,/<\/Directory/{//!d}' /etc/httpd/conf/httpd.conf
    sed -i '/<Directory \/>/a\    Options Indexes FollowSymLinks\n    AllowOverride All\n    Require all granted' /etc/httpd/conf/httpd.conf

    # Restart Apache
    systemctl restart httpd
    echo "${green}Finised LAMP Installation... This is great!${reset}"
    echo ""
}

# Function enable port 80,433 in IPtables
f_open_port () {
    echo "${green}Setting up the firewall for port 80 and 443 inbound!${reset}"
    firewall-cmd --permanent --add-service=http
    firewall-cmd --permanent --add-service=https
    firewall-cmd --permanent --add-port=80/tcp
    firewall-cmd --permanent --add-port=443/tcp
    firewall-cmd --reload
    echo "${green}Completed firewall setup!${reset}"
    echo ""
}

f_install_composer (){
    echo "${green}Starting composer installation... just a sec!${reset}"
    sleep 1

	sudo curl -sS https://getcomposer.org/installer | php
	mv composer.phar /usr/bin/composer
	export PATH=~/.config/composer/vendor/bin:$PATH
	echo 'export PATH=~/.config/composer/vendor/bin:$PATH' >> ~/.bashrc
        export COMPOSER_ALLOW_SUPERUSER=1

    echo ""
    echo "${green}Composer installation completed!${reset}"
    sleep 1
}

f_install_envoy (){
    echo "${green}Starting Envoy installation... just a sec!${reset}"
    sleep 1
co
	composer global require laravel/envoy

    echo ""
    echo "${green}Envoy installation update completed!${reset}"
    sleep 1
}

f_starting_svcs (){
    echo "${green}Starting all other services...${reset}"

	systemctl enable ntpd
	systemctl start ntpd
	systemctl enable crond
	systemctl start crond

}

f_service_checks (){
	if which php > /dev/null; then
	PHPVER=$(php -v|grep --only-matching --perl-regexp "7\.\\d+\.\\d+")
		echo -e "${green}Status: PHP $PHPVER is installed!${reset}\n"
		echo -e "${green}Status: PHP $PHPVER is installed!${reset}\n" >> $LOGFILE 2>&1
	else
		echo -e "${red}Status: PHP was not installed - the script has failed\n";
		echo -e "${red}Status: PHP was not installed - the script has failed\n"; >> $LOGFILE 2>&1
		echo -e  "${red}Thsi is needed to continue. The script will now terminate!!!${reset}\n";
		exit
	fi
	if which httpd > /dev/null; then
		APACHEVER=$(httpd -v | grep version | sed 's/.*://')
		echo -e "${green}Status: $APACHEVER is installed!${reset}\n"
		echo -e "${green}Status: $APACHEVER is installed!${reset}\n" >> $LOGFILE 2>&1
	else
		echo -e "${red}Status: APACHE was not installed - the script has failed\n";
		echo -e  "${red}Thsi is needed to continue. The script will now terminate!!!${reset}\n";
		exit
	fi
	if which mysql > /dev/null; then
		MYSQL=$(mysql --version|awk '{ print $5 }'|awk -F\, '{ print $1 }')
		echo -e "${green}Status: MySQL $MYSQL is installed!${reset}\n"
		echo -e "${green}Status: MySQL $MYSQL is installed!${reset}\n" >> $LOGFILE 2>&1
	else
		echo -e "${red}Status: MySQL was not installed - the script has failed\n";
		echo -e "${red}Status: MySQL was not installed - the script has failed\n"; >> $LOGFILE 2>&1
		echo -e  "${red}Thsi is needed to continue. The script will now terminate!!!${reset}\n";
		exit
	fi

	if which redis-cli > /dev/null; then
		REDIS=$(redis-cli --version|awk '{ print $2 }'|awk -F\, '{ print $1 }')
		echo -e "${green}Status: REDIS $REDIS is installed!${reset}\n"
		echo -e "${green}Status: REDIS $REDIS is installed!${reset}\n" >> $LOGFILE 2>&1
	else
		echo -e "${red}Status: REDIS was not installed - the script has failed\n";
		echo -e "${red}Status: REDIS was not installed - the script has failed\n"; >> $LOGFILE 2>&1
		echo -e  "${red}Thsi is needed to continue. The script will now terminate!!!${reset}\n";
		exit
	fi
	if which supervisord > /dev/null; then
		SUPERVISORD=$(supervisord --version)
		echo -e "${green}Status: SUPERVISORD $SUPERVISORD is installed!${reset}\n"
		echo -e "${green}Status: SUPERVISORD $SUPERVISORD is installed!${reset}\n" >> $LOGFILE 2>&1
	else
		echo -e "${red}Status: SUPERVISORD was not installed - the script has failed\n";
		echo -e "${red}Status: SUPERVISORD was not installed - the script has failed\n"; >> $LOGFILE 2>&1
		echo -e  "${red}Thsi is needed to continue. The script will now terminate!!!${reset}\n";
		exit
	fi

}

f_deploy_login_script () {
    wget https://www.rconfig.com/downloads/rconfig5_centos_login.sh -O /etc/profile.d/login.sh >> $LOGFILE 2>&1
}

f_mysql_secure_setup (){
    echo -e "${magenta}rConfig system installation is almost complete...\r"
    echo -e "Your final task will be to setup the Database Server.\r"
    echo -e "Once the Database Setup wizard is complete, Please REBOOT your server.${reset}";
    sleep 5

    # mysql_secure_installation
    MYSQLSETUPMSG="mysql_secure_installation wizard"
    echo -e "The MySQL setup wizard will now launch\r"
    echo -ne '....'
    sleep 1
    echo -ne '.........'
    sleep 1
    echo -ne '..............'
    sleep 1
    echo -ne '...................'
    sleep 1
    sleep 3
    echo "<<<< Start - $MYSQLSETUPMSG >>>>"  >> $LOGFILE 2>&1
    mysql_secure_installation
}

# The sub main function, use to call neccessary functions of installation
f_sub_main () {
    echo "${green}OS Installation script starting...!${reset}"
    f_disable_selinux
    f_update_os
    f_install_base_pkgs
    f_install_redis_supervisor
    f_install_lamp
    f_open_port
    f_install_composer
    f_install_envoy
    f_starting_svcs
    f_service_checks
    f_deploy_login_script
    f_mysql_secure_setup

    echo ""
    echo ""
    echo "${green}Installation complete, a reboot is always a good idea at this point. ${reset}"
    echo "${green}Go back to the rConfig 5 documentation and follow the instructions to continue the deployment. ${reset}"
    sleep 1
}


# The main function
f_main () {
    f_check_root
}
f_main

exit
